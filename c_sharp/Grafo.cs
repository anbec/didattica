using System;

/*
 * Classe Nodo di lista di adiacenza
 */
class Nodo {
	public string Nome;
	public int Peso;
	public Nodo Next;
	
	public Nodo(string pNome) {
		Nome = pNome;
		Peso = 0;
		Next = null;
	}
	
	public void setPeso(int pPeso) {
		Peso = pPeso;
	}
}

/*
 * Classe Nodo di LLA (Lista di Liste d'Adiacenza)
 */
class NodoLLA {
	public string Nome;
	public Nodo Next;
	public NodoLLA NextLLA;
	
	public NodoLLA(string pNome) {
		Nome = pNome;
		Next = null;
		NextLLA = null;
	}
}

class Grafo {
	private NodoLLA Testa;
	
	public Grafo() {
		Testa = null;
	}
	
	/*
	 * Controlla se il grafo è vuoto (True) o no (False)
	 */
	public bool TestVuoto() {
		if (Testa == null) {
			return true;
		}
		return false;
	}
	
	/*
	 * Aggiunge un nodo al grafo
	 */
	public void AggiungiNodo(NodoLLA n) {
		if (TestVuoto()) {
			Testa = n;
			Testa.Next = null;
			Testa.NextLLA = null;
		} else { //aggiunge in testa alla lista principale
			n.NextLLA = Testa;
			n.Next = null;
			Testa = n;
		}
	}
	
	/*
	 * Controlla se il un nodo con informazione uguale a quella passata come 
	 * argomento è presente nel grafo
	 */
	public bool TestNodoValido(string nome) {
		NodoLLA appo = Testa;
		while (appo != null) {
			if (appo.Nome.Equals(nome)) {
				return true;
			}
			appo = appo.NextLLA;
		}
		return false;
	}
	
	/*
	 * Controlla se l'arco tra i nodi contenenti le informazioni passate come 
	 * argomento esiste (true) oppure no. Viene restituito false anche nel caso
	 * non esista uno dei nodi specificati.
	 */
	public bool TestArcoValido(string src, string dest) {
		if (TestNodoValido(src) == false || TestNodoValido(dest) == false) {
			return false;
		}
		
		NodoLLA appo = Testa;
		while (appo != null) {
			if (appo.Nome.Equals(src)) {
				Nodo appo2 = appo.Next;
				while (appo2 != null) {
					if (appo2.Nome.Equals(dest)) {
						return true;	//è stato trovato l'arco
					}
					appo2 = appo2.Next;
				}
				return false;	//non è stato trovato l'arco
			}
			appo = appo.NextLLA;
		}
		return false; //non è stato trovato il nodo di partenza
	}
	 
	/*
	 * Aggiunge un arco tra i nodi contenenti le stringhe passate come 
	 * argomento. Restituisce true se l'inserimento avviene correttamente, 
	 * false altrimenti.
	 */
	public bool AggiungiArco(string src, string dest) {
		if (TestNodoValido(src) == false || TestNodoValido(dest) == false ||
			TestArcoValido(src, dest) == true) {
			return false;	//src o dest non presenti o arco già presente
		}
		NodoLLA appo = Testa;
		while (appo.Nome.Equals(src) != true) {
			appo = appo.NextLLA;
		}
		Nodo n = new Nodo(dest);
		if (appo.Next == null) {
			n.Next = null;
		} else {
			n.Next = appo.Next;
		}
		appo.Next = n;
		return true;
	}
	
	/*
	 * Stampa di prova. Scrive tutta la LLA
	 */
	 public void Stampa() {
	 	NodoLLA appo = Testa;
	 	Nodo appo2;
	 	while (appo != null) {
	 		Console.Write("Nodo {0}: ", appo.Nome);
			appo2 = appo.Next;
			while (appo2 != null) {
				Console.Write(appo2.Nome);
				Console.Write(' ');
				appo2 = appo2.Next;
			}
			Console.WriteLine();
			appo = appo.NextLLA;
		}
	}
}

class TestGrafo {
	public static void Main(string[] args) {
		Grafo g = new Grafo();
		NodoLLA n = new NodoLLA("A");
		g.AggiungiNodo(n);
		n = new NodoLLA("B");
		g.AggiungiNodo(n);
		g.AggiungiArco("A", "B");
		g.Stampa();
	}
}
